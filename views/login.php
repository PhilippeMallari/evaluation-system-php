<?php  
	
	require_once '../partials/header.php';

	function getTitle() {
		return "Login Page";
	}

?>

	<section class="container">

		<div class="row">
			<div class="col-md-5 mx-auto mt-5">

				<div class="card card-form shadow">

					<div class="card-header">
						<h2 class="text-center card-title"> Login </h2>
					</div>
					
					<form action="../controllers/authenticate.php" method="POST">

						<div class="card-body">
							<div class="form-group form-group-label position-relative e-tooltip-container">

								<?php if (isset($_SESSION['errors']['account'])): ?>
									<small class="alert alert-danger e-tooltip" role="alert">
										<?= $_SESSION['errors']['account']?>	
									</small>
								<?php endif ?>
								<input type="text" id="user" name="user" class="form-control" placeholder="Email"
								<?php /*to retain the form values*/ ?>
								<?php if (isset($_SESSION['form']['username'])): ?>
									value=<?= $_SESSION['form']['username']; ?>
								<?php endif ?>>
								<label for="user">Username or Email </label>
								
							</div>

							<div class="form-group form-group-label position-relative e-tooltip-container <?php echo $_SESSION['errors']['password']? 'mt-5' :  ''; ?>">

								<?php if (isset($_SESSION['errors']['password'])): ?>
									<small class="alert alert-danger e-tooltip" role="alert">
										<?= $_SESSION['errors']['password']?>	
									</small>
								<?php endif ?>
								<input placeholder="Password" type="password" id="password" name="password" class="form-control"
								<?php /*to retain the form values*/ ?>
								<?php if (isset($_SESSION['form']['password'])): ?>
									value=<?= $_SESSION['form']['password']; ?>
								<?php endif ?>>
								<label for="password"> Password </label>
								<small class="alert alert-danger d-none e-tooltip" role="alert">errors</small>
							</div>

							<a href="#">Forgot Password?</a>
						</div>

						<div class="card-footer">
							<button type="submit" class="btn btn-primary btn-block"> Login </button>
						</div>

					</form> <!-- end form -->

				</div>

			</div> <!-- end cols -->
		</div> <!-- end row -->
	</section> <!-- end container -->

<?php require_once '../partials/footer.php'; unset($_SESSION['errors']); unset($_SESSION['form'])?>
