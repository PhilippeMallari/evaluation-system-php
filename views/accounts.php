<?php 
	require_once '../partials/header.php';

	function getTitle() {
		return "Home";
	}

	$accounts_query = "SELECT * FROM accounts";
	$accounts =  mysqli_query($conn, $accounts_query);

 ?>


<section class="container mt-3">

	<div class="row">
		<div class="col-md-8 mx-auto">
			<div class="table-responsive">
				<table class="table  table-hover">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">First Name</th>
							<th scope="col">Last Name</th>
							<th scope="col">Email</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($accounts as $key => $account): ?>
							
						<tr>
							<th scope="row"><?= $key + 1 ?></th>
							<td><?= $account['first_name'] ?></td>
							<td><?= $account['last_name'] ?></td>
							<td><?= $account['email'] ?></td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			
		</div>
		<div class="col-md-2"></div>
	</div>

</section>

<?php require_once '../partials/footer.php'; ?>

