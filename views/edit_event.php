<?php  
	
	require_once '../partials/header.php';

	function getTitle() {
		return "Admin Dashboard";
	}

?>
	<?php

	$event_to_be_edited = $_GET['id'];

	
	$event_query = "SELECT name, description, rating, image FROM events WHERE id = $event_to_be_edited";
	$event_result = mysqli_query($conn, $event_query);
	$event = mysqli_fetch_assoc($event_result);
	

	?>
	<section class="container-fluid">
		<div class="row">
			<div class="col-md-8 mx-auto">
				<div class="card create-event-form">

					<div class="card-header">
						<h2 class="text-center card-title"> Update an Event </h2>
					</div>

					<form action="../controllers/update_event.php" method="POST" enctype="multipart/form-data">
						
						<div class="card-body">
							<input type="hidden" name="event_id" class="form-control" value="<?php echo $event_to_be_edited;?>">
							<div class="form-group position-relative">
								<input type="text" id="eventName" name="eventName" class="form-control" placeholder="Email" value="<?php echo $event['name'] ?>">
								<label for="eventName">Event Name </label>
							</div>

							<div class="form-group position-relative">
								<input placeholder="text" id="eventDescription" name="eventDescription" class="form-control" value="<?php echo $event['description'] ?>">
								<label for="eventDescription"> Event Description </label>
							</div>

							<div class="form-group position-relative">
								<input placeholder="text" id="eventRating" name="eventRating" class="form-control" value="<?php echo $event['rating'] ?>">
								<label for="eventRating"> Event Rating </label>
							</div>

							<div class="form-group position-relative">
								<input type="file" placeholder="text" id="eventImage" name="eventImage" class="form-control" value="<?php echo $event['image'] ?>">
								<label for="eventImage"> Event Image </label>
							</div>
						</div>

						<div class="card-footer">
							<button type="submit" class="btn btn-primary btn-block">Update Event</button>
						</div>
					</form>
				</div>
			</div> <!-- end column -->
		</div> <!-- end row -->
	</section> <!-- end container -->

<?php require_once '../partials/footer.php'; ?>