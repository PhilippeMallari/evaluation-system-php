
<nav class="navbar navbar-expand-lg navbar-dark bg-primary" id="navBar">
	<button class="navbar-toggler ml-0" type="button" data-toggle="collapse" data-target="#sbNavList">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="sbNavList">
		<ul class="navbar-nav py-2 mr-auto">
			<li class='nav-item px-3'>
				<a class='nav-link' href='./register.php'> Register </a>
			</li>	
		</ul>

		<ul class="navbar-nav ml-auto">

			<?php if (isset($_SESSION['user']['firstName'])): ?>
				
				<li class="nav-item dropdown mr-5">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Welcome, <?= $_SESSION['user']['firstName'] ?>
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="../views/profile.php">Profile</a>
						<a class="dropdown-item" href="#">Settings</a>

					<div class="dropdown-divider"></div>

						<a class="dropdown-item" href="../controllers/logout.php">Logout</a>

					</div>
				</li>

			<?php else: ?>

				<li class='nav-item mr-5'>
					<a class='nav-link' href='../views/login.php'>Login</a>
				</li>

			<?php endif ?>


		</ul>
	</div>
</nav> <!-- end nav -->



